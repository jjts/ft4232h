#include "ftd2xx.h"

#define READ 0x00
#define WRITE 0xFF
#define SPI 0xBF
#define BAUD (460800/2)
#define BUF_LEN 8
#define RECV_BUF 2048
#define BITMODE 0x01



FT_HANDLE	ftHandle[4];
pthread_t thread_channel[4];
unsigned char received_byte[4];
unsigned int received_int[4];
unsigned int thread_en[4];
unsigned char idle[2];
unsigned int read_size[4];
unsigned int receive_ind[4];

void *receive_spi_byte(void *param);
void *receive_spi_int(void *param);
void spi_write_byte(unsigned char addr, unsigned char data, int iport);
void spi_read_byte(unsigned char addr, unsigned char *data, int iport);
void spi_write_int(unsigned char addr, unsigned int data, int iport);
void spi_read_int(unsigned char addr, unsigned int *data, int iport);
int opendev_byte(int *channel, int baud);
int opendev_int(int *channel, int baud);
void closedev(int iport);



